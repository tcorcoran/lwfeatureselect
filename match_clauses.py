#!/usr/bin/python
import json
import os
import string
import re
from glob import glob
from nltk.tree import ParentedTree
from pdb import set_trace


def remove_id(word):
    """Removes the numeric suffix from the parsed recognized words: e.g. 'word-2' > 'word' """
    if not word:
        return word
    return word[0:word.rindex("-")]

def get_id(word):
    """returns the numeric suffix from the parsed recognized words: e.g. 'word-2' > 2 """
    if not word:
        return word
    return int(word[word.rindex("-")+1:])

def clause_reIndexed(text, adjust):
    return [remove_id(x)+'-'+str(adjust + get_id(x)) for x in text]

def get_dependencies(dependencies, clause_words, adjust):
    results = []
    cw = [(get_id(x)+adjust+1) for x in clause_words] # this is confusing and subtle, dependencies start at 1, text starts at 0
    for dep in dependencies:
        if get_id(dep[2]) in cw: 
            results.append(dep)
    #set_trace()
    return results
def get_parseTree(sent_pt, clause_text, adjust, just_put_the_index_on=False):
    sent_pt = ParentedTree.parse(sent_pt)
    for i, pos in enumerate(sent_pt.treepositions('leaves'), start=0):
        sent_pt[pos] = sent_pt[pos] + '-'+str(i)
    
    pat = re.compile(r'\n +')
    if just_put_the_index_on:
        return pat.sub(' ', sent_pt.__str__())

    remove = set(['and', 'so', 'but', 'then', 'since', 'as', 'or', 'because', 'if', 'for', 'yet', 'however',
                  ':', '"', '?', '!', ';', '.', '-', ')', '(', ',', 'when', 'after']) 
    ct_clean = clause_text[:]
    if clause_text[0] in remove:
        ct_clean = ct_clean[1:]
    if clause_text[-1] in remove:
        ct_clean = ct_clean[:-1]
    
    ct_clean = set(ct_clean)
    clause_text = set(clause_text)
    matched = []
    for st in sent_pt.subtrees():
        if not set(st.leaves()).difference(clause_text) or not set(st.leaves()).difference(ct_clean):
            matched.append(st)

    if matched:
        longest = sorted(matched, key = lambda x: len(x.leaves()))[-1]
        for  i, pos in enumerate(longest.treepositions('leaves'), start=get_id(longest.leaves()[0])-adjust):
            longest[pos] = remove_id(longest[pos])
            longest[pos] = longest[pos] + '-' + str(i)

        if (len(clause_text) - len(longest.leaves()))/float(len(clause_text)) > .25: 
            print "CL= %d  M =%d  diff= %d\tper = %f"%(len(clause_text) , len(longest.leaves()), 
                                                       len(clause_text) - len(longest.leaves()), 
                                                       (len(clause_text) - len(longest.leaves()))/float(len(clause_text)) )
    else:
        longest = ''
        print "NO MATCH!!"

    return pat.sub(' ', longest.__str__())         


clause_parses = glob('parsed_as_clauses/json_files/*.json')
#clause_parses = ['/Users/summeraccess/L&W/ML/parsed_as_clauses/json_files/11.json']
for clause_file in clause_parses:
    contained = 0
    with open(clause_file, 'r') as cs:
        clauses = json.load(cs)
        for ct_forIndex in clauses['sentences']:
            for i, word in enumerate(ct_forIndex['text'],):
                ct_forIndex['text'][i] += '-' + str(i)
        #set_trace()
        with open('parsed_as_sentences/json_files/'+os.path.split(clause_file)[1], 'r') as ss:
            # if os.path.split(clause_file)[1] == '11.json':
            #     set_trace()
            mismatch = False
            sentences = json.load(ss)
            new_segmentation = {'clauses': []} 
            for s in sentences['sentences']:
                for i, word in enumerate(s['text'],):
                    s['text'][i] += '-' + str(i)

                adjust = 0 # how many words were in all clauses of this sentence
                clauses['sentences'] = clauses['sentences'][contained:] # take out the clauses which were in the previous sentence
                contained = 0 # how many clauses will be in this sentence
                #print string.join(s['text'], ' ') 
                for C in clauses['sentences']:
                    #set_trace()
                    #print "#########" + string.join(C['text'], ' ')
                    c_end_bound = C['text'][-1]
                    c_begin_bound = C['text'][0]

                    if C['text'] == s['text']: # found exact match, no clause segmentaion needed
                        #set_trace()
                        contained += 1
                        words = s['words']
                        text = [remove_id(x) for x in s['text']]
                        dependencies = s['dependencies']
                        parsetree = get_parseTree(s['parsetree'], None, adjust, just_put_the_index_on=True)
                        bound = 'SS'         #SS = no clause segmentation
                        new_segmentation['clauses'].append({'words': words, 'text': text, 'dependencies':dependencies, 
                                                            'bound': bound, 'parsetree': parsetree })
                        break

                    elif C['text'] == s['text'][:len(C['text'])]:
                        #set_trace()
                        if c_end_bound in s['text']:  
                            words = s['words'][:get_id(c_end_bound) + 1]
                            text = [remove_id(x) for x in C['text']]
                            dependencies = get_dependencies(s['dependencies'], C['text'], adjust )
                            parsetree = get_parseTree(s['parsetree'], C['text'], adjust)
                            bound = 'SC' #             SC = clause segmentation begins last word, sentence begins first word
                            adjust = len(C['text'])
                            contained += 1 
                            if not words:
                                print "Clause mismatch in story: %s"%os.path.split(clause_file)[1]
                                set_trace()
                            new_segmentation['clauses'].append({'words': words, 'text': text, 'dependencies':dependencies, 
                                                                'parsetree': parsetree, 'bound': bound})

                        else:
                            print "potential clause mismatch in story: %s"%os.path.split(clause_file)[1]
                            print "Clause end bound not found in sentence!!"


                    elif clause_reIndexed(C['text'], adjust) == s['text'][-len(C['text']):]:
                        #set_trace()
                        words = s['words'][-len(C['text']):]
                        text = [remove_id(x) for x in C['text']]
                        dependencies = get_dependencies(s['dependencies'], C['text'], adjust )
                        parsetree = get_parseTree(s['parsetree'], clause_reIndexed(C['text'], adjust), adjust ) 
                        bound = 'CS'#            CS = clause segmentations begins first word, sentence ends last word
                        contained += 1
                        adjust += len(C['text'])
                        if not words:
                            print "Clause mismatch in story: %s"%os.path.split(clause_file)[1]
                            set_trace()
                        new_segmentation['clauses'].append({'words': words, 'text': text, 'dependencies':dependencies, 
                                                            'parsetree': parsetree, 'bound': bound})
                        break

                    else:
                        #set_trace()
                        words = s['words'][get_id(c_begin_bound)+adjust:get_id(c_end_bound)+adjust]
                        text = [remove_id(x) for x in C['text']]
                        dependencies = get_dependencies(s['dependencies'], C['text'], adjust )
                        parsetree = get_parseTree(s['parsetree'], clause_reIndexed(C['text'], adjust), adjust)
                        
                        bound = 'CC' #             CC = clause boundries on both first/last word
                        adjust += len(C['text'])
                        contained += 1
                        if not words:
                            print "Clause mismatch in story: %s"%os.path.split(clause_file)[1]
                            set_trace()
                        new_segmentation['clauses'].append({'words': words, 'text': text, 'dependencies':dependencies, 
                                                            'parsetree': parsetree, 'bound': bound})


                    if len(clauses['sentences']) == 1:
                        if mismatch:
                            # if we get here, it means that we've looked gone though all the clauses
                            # and there's still sentences left to be segmented
                            print "clause mismatch in story: %s"%os.path.split(clause_file)[1]
                            set_trace()
                        mismatch = True

            #set_trace()
    with open('matched_parses/' +os.path.split(clause_file)[1], 'wb') as fo:
        json.dump(new_segmentation, fo)


