#!usr/bin/python

import csv, csvUnicode, json, os, string, re
from math import floor
from glob import glob
from pdb import set_trace
from operator import itemgetter
from collections import OrderedDict, defaultdict
from itertools import groupby
from random import randint
from nltk.tree import ParentedTree
from nltk.stem.porter import PorterStemmer
stemmer = PorterStemmer()

with open('LIWC_words.json', 'rb') as fi:
    liwc_words = json.load(fi)

def remove_id(word):
    """Removes the numeric suffix from the parsed recognized words: e.g. 'word-2' > 'word' """
    if not word:
        return word
    return word[0:word.rindex("-")]

def get_id(word):
    """returns the numeric suffix from the parsed recognized words: e.g. 'word-2' > 2 """
   # set_trace()
    if not word:
        return word
    return int(word[word.rindex("-")+1:])

def findBoundTypes(clause):
    if clause['bound'].startswith('S'):
        begin = '0'
    else:
        begin = '1'
    if clause['bound'].endswith('S'):
        end = '0'
    else:
        end = '1'
    return begin, end

def findFirstPerson(clause):
    cues = set(['i', 'me', 'my', 'mine', 'myself', 'we', 'us', 'ours', 'our'])
    for w in clause['text']:
        if w.lower() in cues:
        	return 1
    return 0

def findSecondPerson(clause):
    cues = set(['you', 'your', 'yours', 'youve', 'youd', 'youll', 'yall', "y'all"])
    for w in clause['text']:
        if w.lower() in cues:
        	return 1
    return 0


def findCopula(clause):
    for dep in clause['dependencies']:
        if dep[0] == 'cop':
        	return 1
    return 0

def findTime(clause):
    time_tags = set(['DATE', 'TIME', 'DURATION'])
    cues = set(('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'january', 'february', 
                'march', 'april', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'yesterday', 
                'today', 'tomorrow', 'day','days', 'months', 'years','month', 'year'))
    for w in clause['words']:
        try:
            if w[1]['NamedEntityTag'] in time_tags:
                return 1
        except: set_trace()
        if w[1]['Lemma'].lower() in cues:
            return 1
    return 0

def findModal(clause):
    cues = set(['can', 'couldve', 'could', 'shoulda', 'coulda', 'woulda', 'gonna', 'gotta', 'will', 'itll', 'may', 'might', 
                'mightve', 'must', 'mustve', 'ought', 'oughta', 'shall', 'should', 'shouldve', 'thatll', 'theyll', 'wholl', 
                'would', 'wouldve','couldnt', 'mustnt', 'oughtnt', 'shant', 'shouldnt', 'wouldnt'])
    for w in clause['words']:
        if w[1]['Lemma'].lower() in cues or w[1]['PartOfSpeech'] == 'MD':
        	return 1
    return 0 

def findQuestion(clause):
    if '?' in set(clause['text']):
        return 1
    return 0

def findWHWord(clause):
    cues = set(('who', 'what', 'when', 'where', 'why', 'how',))
    for w in clause['words']['Lemma']:
        if w in cues:
            return 1
    return 0


def findConditional(clause):
    if 'if' in set([x.lower() for x in clause['text']]):
        return 1
    return 0

def findQuote(clause):
    # `` == left quote
    # "" == right quote
    if "``" in clause['text'] and "\'\'" in clause['text']:
        l = clause['text'].index("``")
        r = clause['text'].index("\'\'")
        if r-l >= len(clause['text'])/2:
        	return 1
    elif clause['text'][0] in ["``", "\'\'"] or clause['text'][-1] in ["``", "\'\'"]:
        #clause bounded by quotes. Sometimes quotes are broken apart across clause boundries
        return 1
    return 0

def findImperative(clause):
    # this is actually tricky now since in some coordinated clauses the subject has been elided
    
    if clause['bound'].startswith('C'): # clause has been segmented, with the change of an elided subject. Skip
        return 0

    if clause['words'][0][1]['PartOfSpeech'].startswith('VB') and '?' != clause['text'][-1]:
        return 1
    else:
        return 0

def findNegate(clause):
    cues = set(["n't", 'not', "ain't", 'aint', 'arent', 'cannot', 'cant', 'couldnt', 'didnt', 'doesnt', 'dont', 'hadnt', 'hasnt',
                'havent', 'isnt', 'mustnt', 'neednt','neither', 'never', 'no', 'nobody', 'none', 'nor', 'nothing', 'nothings', 
                'nowhere', 'oughtnt', 'shant', 'shouldnt', 'wasnt', 'werent', 'without', 'wont', 'wouldnt'])
    for w in clause['words']:
        if w[1]['Lemma'].lower() in cues:
        	return 1
    return 0

def findIndefArticle(clause):
    cues = set(['a', 'an', 'some'])
    for w in set(clause['text']):
        if w.lower() in cues:
        	return 1
    return 0

def findConnective(clause):
    cues = set(['then', 'so', 'but','as', 'next', 'after','afterward', 'afterwards', 'before', 'when', 'while', 'during'])
    for w in set(clause['text']):
        if w.lower() in cues:
        	return 1
    if 'and' == clause['text'][0].lower():
        return 1
    return 0

def findLIWC(clause, cat):
    """Return 1 if some dobj or verb is in LIWC cat. Where cat is cogmech, affect, or motion"""
    target_words = [w[1]['Lemma'].lower() for w in clause['words'] if w[1]['PartOfSpeech'].startswith('VB')]
    for dep in clause['dependencies']:
        if dep[0] == 'dobj':
        	target_words.append(remove_id(dep[2]).lower()) 
        if dep[0] == 'cop': # dobjs of cop are not labeled dobj
        	target_words.append(remove_id(dep[2]).lower()) # same for possesive have (we will get some dups of verbs, but thats OK)
        if remove_id(dep[1]).lower() in ['have', 'has', 'had', 'having', 'havin']:
        	target_words.append(remove_id(dep[2]).lower())

    target_words = set(target_words)
    cues_exact = set(liwc_words[cat]['exact'])
    
    for w in target_words: # these are exact matches
        if w in cues_exact:
        	return 1

    cues_root = set(liwc_words[cat]['template']) # these are LIWC words stored as stems
    for w in target_words:
        for c in cues_root:
        	if w.startswith(c[:-1]):
        		return 1
    return 0

def findThirdPersonStat(clause, firstperson, secondperson):
    if firstperson or secondperson:
        return 0
    for w in clause['words']:
        if w[1]['PartOfSpeech'] == 'EX':
        	return 1
    return 0

def findThirdPersonProg(clause, firstperson, secondperson):
    if firstperson or secondperson:
        return 0
    deps = set([x[0] for x in clause['dependencies']])
    for w in clause['words']:
        if w[1]['Lemma'].lower() == 'be' and 'cop' not in deps:
        	return 1

    for i, w in enumerate(clause['words']):
        if w[1]['Lemma'].lower() == 'have':
        	# check if have is followed by verb, we don't want possesive have
        	if len(clause['words'][i:]) >= 2 and clause['words'][i + 1][1]['PartOfSpeech'].startswith('VB'): 
        		return 1
        	# if its the last word then maybe VP has been elided
        	# hard to do this with NPs
        	elif len(clause['words'][i:]) == 1:
        		return 1
        # if first word is have, maybe we have a question in the progressive
        # (we don't normally see SAI with have in AE)
        if i == 0 and w[1]['Lemma'].lower() == 'have':
        	return 1

    return 0    	

def StativeVerb(clause):
    cues = set(['agree', 'suspect', 'appear', 'believe', 'belong', 'concern', 'consist', 'contain', 'depend', 'deserve', 
                'disagree', 'dislike', 'doubt', 'feel', 'felt', 'fit ','fitted ' 'hate', 'hear', 'heard', 'imagine', 
                'impress', 'include', 'involve', 'know', 'knew', 'like', 'love', 'matter', 'measure', 'mind', 'need', 
                'owe', 'own', 'prefer', 'promise', 'realise', 'recognise', 'remember', 'seem', 'sound', 'suppose', 
                'surprise', 'understand', 'want', 'weigh', 'wish'])
    for w in clause['words']:
        if w[1]['Lemma'].lower() in cues:
        	return 1
    return 0

def findStems(clause, feature_vector, clause_type):
    for word in clause['words']:
        word_stem = clause_type + stemmer.stem_word(word[1]['Lemma']).lower()
        if word_stem not in feature_vector:
            feature_vector[word_stem] = 1
    #    if word_stem in ['0', 0, 'o', 'O']:
    #    	set_trace()
    return feature_vector

def findPOS(clause, feature_vector, clause_type):
    for word in clause['words']:
        if clause_type+ word[1]['PartOfSpeech'] not in feature_vector:
        	feature_vector[clause_type+word[1]['PartOfSpeech']] = 1
    return feature_vector

def splitDEP(clause):
    if not clause['parsetree']:
        return [clause], []
    tree = ParentedTree.parse(clause['parsetree'])
#    for i, pos in enumerate(tree.treepositions('leaves'), start=0): # Concat indexes to words(leaves)
#        tree[pos] = tree[pos] + '-'+str(i)

#    set_trace()
    # Find all subtrees in tree starting with S, but not the root S
    # We're looking for: SBAR, SQ, SBARQ, S
    S_deps = []
    # This block is where the heuristics for seperating clauses are
    for st in tree.subtrees():
        if st.parent: 
            if st.parent.node != 'ROOT':
                if st.parent.parent and st.parent.parent.node != 'ROOT':
                    if st.node.startswith('S'):
                        if tree.root().node == 'ROOT':
                            if tree.root()[0].startswith('S'):
                                if st.pos()[0][1] not in ['TO', 'VBG']: # This will keep indef and progressive verbs with the main VP
                                    S_deps.append(st)
                            
                if st.parent.node == 'PP' and st.node == 'IN':
                    if st.parent.leaves() != tree.leaves():
                        S_deps.append(st.parent)
            else:
                if st.node.startswith('CC') and st.leaves() !=  tree.leaves():  # Add CCs (which don't govern the whole clause)
                    S_deps.append(st)
                elif st.node.startswith('W') and st.leaves != tree.leaves():
                    S_deps.append(st)
                elif st.node == 'PP' and st.leaves() != tree.leaves():# May be adjoined PP
                    for isIN in st.subtrees():
                        if isIN.node  == 'IN': # Marks subordinate clause
                             S_deps.append(st)
                        break 
                    if st.right_sibling and st.right_sibling.startswith('S'): # try to make this the INDEP clause now
                        tree[st.right_sibling.treeposition()].node  = '#!!#' #change node so we don't catch it later
                        for isS in st.right_sibling.subtrees():
                            if isS.startswith('S'):
                                tree[isS.treeposition()].node = '#!!#'
                            break
#    set_trace()
    # remove duplicate leaves (there may be several nodes that map to the same leaves)
    S_deps_leaves = [x.leaves() for x in S_deps]
    #set_trace()
    all_deps = list(set([frozenset(x) for x in S_deps_leaves]))
   # set_trace()
    deps_dups_removed = all_deps[:]
    for i in all_deps:
        for j in all_deps:
        	if set(i).issubset(set(j)) and i != j:
        		deps_dups_removed.remove(i)
        		break
    
#    set_trace()
    # resort the lists of leaves so they're in the original order
    deps_dups_removed = [sorted(list(i), key= lambda x: get_id(x)) for i in deps_dups_removed]
    deps = [] #
    indep_ranges = range(len(clause['text']))
    for i in sorted(deps_dups_removed, key = lambda x: get_id(x[0]), reverse = True):
    # use reversed list so we can take slices from indep_ranges without screwing around with indexes
    # note that the the leaves themselves are not reversed (and should be in their true order)
        begin = get_id(i[0])
        if len(i) > 1:
        	end = get_id(i[-1]) + 1 # Add 1 to accounting for that these are inclusive boundries and python list slices aren't
        else:
        	end = None
# TODO:use parsetree to smartly recombine indep phrases
#      e.g. John and I, while waiting for the bus, saw a rat crawl under bench.
#      This will be 2 seperate entries for indep phrases--it should really be one.
        indep_ranges = indep_ranges[:begin] + indep_ranges[end:]
        dependencies = findDependencies(clause['dependencies'], begin, end)		
        deps.append({'text':clause['text'][begin:end], 'words':clause['words'][begin:end], 
                     'dependencies':dependencies, 'bound':clause['bound']})
    
    # here we group consecutive numbers so we can use the tuples to grab the independent part clause
#    set_trace()
    indep_tups = []
    for k, g in groupby(enumerate(indep_ranges), lambda (i,x):i-x):
            group = map(itemgetter(1), g)
            indep_tups.append((group[0], group[-1]))
    indeps = []
    for first,last in indep_tups:
        if first != last:
            dependencies = findDependencies(clause['dependencies'],first, last)
            indeps.append({'text': clause['text'][first:last+1], 'words':clause['words'][first:last+1], 
                           'dependencies':dependencies, 'bound': clause['bound']})
        else:
            
            dependencies = findDependencies(clause['dependencies'], first, last)
            text = clause['text'][first]
            if text in set(['"','``', "'", ',', ':', ';', '!', '.', '-','-RRB-', '?', ')', '...']):
                try: 
                    indeps[-1]['text'].append(text)
                except IndexError: 
                    indeps.append({'text': text, 'dependencies': dependencies, 'words': [], 'bound': clause['bound']})
                indeps[-1]['dependencies'].extend(dependencies)
                try:
                    words = clause['words'][first]
                    indeps[-1]['words'].append(words)
                except IndexError:
                    continue 
     
            else:    
                try:
                    indeps.append({'text': [clause['text'][first]], 'words': [clause['words'][first]], 
                               'dependencies': dependencies, 'bound': clause['bound'] })
                except:
                    set_trace()

    return indeps,deps

def findDependencies(dependencies,begin, end):
    result = []
    if not end or begin == end:
        r = [begin + 1]
    else:
        r = range(begin+1, end+2)
    for dep in dependencies:
        if get_id(dep[2]) in r:
        	result.append(dep)
    return result 

def makeVector(clause, FV, clause_type):
  
    if FV[clause_type+'FirstPerson'] != 1:
        FV[clause_type+'FirstPerson'] = findFirstPerson(clause)
    if FV[clause_type+'SecondPerson'] != 1:
        FV[clause_type+'SecondPerson'] = findSecondPerson(clause)
    if FV[clause_type+'Copula'] != 1:
        FV[clause_type+'Copula'] = findCopula(clause)
    if FV[clause_type+'Time'] != 1:    
        FV[clause_type+'Time'] = findTime(clause)
    if FV[clause_type+'Modal'] != 1:
        FV[clause_type+'Modal'] = findModal(clause)
    if FV[clause_type+'Question'] != 1:
        FV[clause_type+'Question'] = findQuestion(clause)
    if FV[clause_type+'Negate'] != 1:
        FV[clause_type+'Negate'] = findNegate(clause)
    if FV[clause_type+'IndefArticle'] != 1:
        FV[clause_type+'IndefArticle'] = findIndefArticle(clause)
    if FV[clause_type+'Connective'] != 1:
        FV[clause_type+'Connective'] = findConnective(clause)
    if FV[clause_type+'LIWC_Cogmech'] != 1:
        FV[clause_type+'LIWC_Cogmech'] = findLIWC(clause, 'cogmech')
    if FV[clause_type+'LIWC_Affect'] != 1:
        FV[clause_type+'LIWC_Affect'] = findLIWC(clause, 'affect')
    if FV[clause_type+'LIWC_Motion'] != 1:
        FV[clause_type+'LIWC_Motion'] = findLIWC(clause, 'motion')
    if FV[clause_type+'ThirdPersonStat'] != 1:
        FV[clause_type+'ThirdPersonStat'] = findThirdPersonStat(clause, FV[clause_type+'FirstPerson'], FV[clause_type+'SecondPerson'])
    if FV[clause_type+'Imperative'] != 1:
        FV[clause_type+'Imperative'] = findImperative(clause)
    if FV[clause_type+'ThirdPersonProg'] != 1:
        FV[clause_type+'ThirdPersonProg'] = findThirdPersonProg(clause, FV[clause_type+'FirstPerson'], FV[clause_type+'SecondPerson'])
    if FV[clause_type+'StativeVerb'] != 1:
        FV[clause_type+'StativeVerb'] = StativeVerb(clause)    
    if FV[clause_type+'Conditional'] != 1:
        FV[clause_type+'Conditional'] = findConditional(clause)    
   # FV = findStems(clause, FV, clause_type)
   # FV = findPOS(clause, FV, clause_type)
    return FV

def changeKeys(d, clause_type):
      if type(d) is dict:
              return dict([(clause_type+k, changeKeys(v, clause_type)) for k, v in d.items()])
      else:
              return d

def makeNonClauseTypeFeatures(whole_clause, whole_clause_vector, story_length, clause_position):
    
    if clause_position == 1:
        whole_clause_vector['FirstClause'] = 1
    else:
        whole_clause_vector['FirstClause'] = 0
    if clause_position == story_length:
        whole_clause_vector['LastClause'] = 1
    else:
        whole_clause_vector['LastClause'] = 0

    whole_clause_vector['Quote'] = findQuote(whole_clause)

    bin_pos = int( float(clause_position-1)/float(story_length-1+.001)*10)
    whole_clause_vector['BinPosition'+str(bin_pos)] = 1
    if not clause['parsetree']:
        whole_clause_vector['Tree'] = clause['parsetree']
    else:
        sent_pt = ParentedTree.parse(clause['parsetree'])
        for pos in sent_pt.treepositions('leaves'):
            sent_pt[pos] = remove_id(sent_pt[pos])
        pat = re.compile(r'\n +')
        whole_clause_vector['Tree'] = pat.sub(' ', sent_pt.__str__())

    whole_clause_vector['BoundBegin'], whole_clause_vector['BoundEnd']  = findBoundTypes(whole_clause)    
    #whole_clause_vector = findStems(whole_clause, whole_clause_vector) # These are in the clause typed features now
    #whole_clause_vector = findPOS(whole_clause, whole_clause_vector)   

    return whole_clause_vector



def getLabel(annotations, CV):    
    if annotations[-1] == '1':
        CV['Label']= annotations[randint(0,2)]
    else:
        CV['Label'] =  max(set(annotations[:-1]), key=annotations.count)
    
    CV['Agreement'] = annotations[-1]
    return CV


# get headers (lemma, clause position)
def getHeaders(feature_vector,  headers):
    for k in feature_vector:
        if k not in headers and k not in ['Label', 'Agreement', 'Tree', 'BoundBegin', 'BoundEnd']:
        	headers.append(k)
    return headers

def doPrependedVector(clause, labels, story_length, clause_position, headers):
    clause_vector = defaultdict(int)
    i,d = splitDEP(clause)
    
    for indep_clause, dep_clause in map(None, i, d):
        if indep_clause:
            clause_vector = makeVector(indep_clause, clause_vector, 'INDEP-')
        if dep_clause:
            clause_vector = makeVector(dep_clause, clause_vector, 'DEP-')
   
    clause_vector = makeNonClauseTypeFeatures(clause, clause_vector, story_length, clause_position)
    clause_vector  = getLabel(labels, clause_vector)
    headers = getHeaders(clause_vector, headers)
    return clause_vector, headers

stories_dir = 'matched_parses/'
stories = [ stories_dir + str(x) + '.json' for x in range(1,51)] 
with open('BS_Annotation_Compilation_TestTrainDev.csv', 'r') as labels_in:
    r= csv.reader(labels_in, delimiter = '\t')
    r.next()
    for sf in stories:
        headers = [ ]
        story_vectors = []
        with open(sf, 'rb') as fi:
            story = json.load(fi)
        clause_position = 1
        story_length = len(story['clauses'])
        for clause in story['clauses']:
            labels = r.next()
           # clause_vector = makeVector(clause, clause_position, story_length)
#                set_trace()
           # clause_vector = getLabel(labels[3:7], clause_vector)
           # story_vectors.append(clause_vector)
           # headers = getHeaders(clause_vector, headers)
            clause_vector, headers = doPrependedVector(clause, labels[3:7], story_length, clause_position, headers)
            story_vectors.append(clause_vector)
            clause_position += 1
            i,d = splitDEP(clause)
            print u'*******'*6
            print u"PARSE: " + clause['parsetree']
            print u"SENT: " + string.join(clause['text'], ' ')
            if not i:
                set_trace()
            for j,k in map(None, i,d):
                if j: print u'IN: ' + ' '.join(j['text'])
                if k: print u'DEP: ' + ' '.join(k['text'])
                
        with open("results/" + labels[0] +'/' + os.path.splitext(os.path.split(sf)[1])[0]+'.csv', 'wb') as fo:
            wo = csvUnicode.UnicodeWriter(fo)
            headers = sorted(headers) 
            headers.insert(0, 'Label')
            headers.insert(1, 'Agreement')
            headers.insert(2, 'Tree')
            headers.insert(3, 'BoundBegin')
            headers.insert(4, 'BoundEnd')
            wo.writerow(headers)
#        		set_trace()
            for row in story_vectors:
                line = ['0'] * len(headers) # fill row with zeros
                for i, feature in enumerate(headers):
                    if feature in row:  # check for all the features in this clause
                        if isinstance(row[feature], int):
                            line[i] = str(row[feature])
                        else:
                            line[i] = row[feature]
                line[0],line[1], line[2] = row['Label'], row['Agreement'], row['Tree']
                line[3], line[4]  =  row['BoundBegin'], row['BoundEnd']
                #print line
                wo.writerow( line )

