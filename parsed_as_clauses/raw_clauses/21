Yesterday I attended a mothers' group playdate.
Formally it was an "ice cream social."
A big fan of ice cream, I didn't intend to miss it, even though my child is too young to play with the happy, babbling, walking toddlers.
My child was, after pitching one of his usual fits about being put into the car seat for the fifteen minutes it took to drive there, finally, blissfully asleep in his stroller (read: torture chamber), so I had hands and, for a moment, mind free.
The mothers who turned up all seemed like women I could relate to, would love to get to know, and would find interesting--the type who have opinions about social issues and like to read.
I was eager to dig in and get to know them.
Unfortunately for my social craving, each woman was busy being a mother--which meant following behind their toddlers to keep them from tripping into the street, or retrieving a ball that was sent flying into the next yard, or scooping ice cream into tiny mouths.
I felt disappointed.
I ate my indulgent bowl of ice cream with extra jimmies and caramel sauce silently.
I asked a few questions,
made a few comments,
and was politely answered before that particular mother had to return to her task of being absolutely available to her child.
And as I sat there in my sort of smugness, feeling disappointed not to have discussion with what I knew to be intelligent, interesting women--
my own child struck up a caterwauling so loud that I was barely heard shouting over him that I would go walk him in his stroller to quiet him.
The stroller walk was a temporary balm.
Feeding him even more temporary.
Changing him had almost no effect at all.
And so I got up with apologies, barely heard over his plaintive wailing, and madly dashed for my car, trying to speak reasonably to my child that we would soon be home.
That his suffering was nearly over.
That if he could just hang in there a few minutes more, the lovely rythm of the car would lull him to sleep or at least calm.
And in my haste I realized that I was in exactly the same position as those other mothers.
That intelligent conversation from my end would also have to wait.
