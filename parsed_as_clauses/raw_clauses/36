Jump to the full entry travel map Hangzhou, Xitang, and Shanghai, Zhejiang, China
(Disclaimer: long entry but, hey, at least we kept the old folks busy!) 
Hangzhou quickly became our most familiar city in China as we were drawn there for reasons of proximity, transit, beauty, camera repair, and, most importantly, cheese.
That being said, there was plenty we had yet to see and plenty we were happy to see again,
this time accompanied by the ever open-eyed and always impressed Tempest parents.
After triple jumping from the airport to our youth hostel on the relaxed shores of China's most famous West Lake and some strolling along the waterfront walking corridor, it was time to retire for the evening before facing the myriad sights of the 5.5 million population city, one which the Chinese are rightfully proud of and joyful to experience.
&nbs;
First up on the culture express was a visit to one of mainstream China's (read: non-Tibetan) most notable temples, the Lingyin Temple.
Since our experience at the Lama Temple in Beijing, we hadn't truly highlighted any temples,
but this one was definitely worth it.
Dove-tailing paths along the Fei Lang Peak ('Peak Flying From Afar') wound past Buddhist some neat Buddhist carvings in the rock faces which date from the 10th to 14th Century, a period which includes Hangzhou's run as capitol city during the Southern Song Dynasty.
The temple complex was composed of a smattering of towering but somehow unimposing halls.
The solid mustard-yellow walls divided by green pillars and dotted with delicately latticed and decorated windows was quite inviting.
The colors were much less confusing and cluttered than those of the Lama Temple in Beijing
and the overall feel of the complex much more genuine.
This was the first and only temple experience for the parents in the party, who came away thoroughly impressed and not yet 'templed-out'.
&nbs;
The rest of the afternoon included the botanical gardens, whowere seasonally in a bit of an awkward phase although the presence of magnolias, some lotus flowers, and even a tananger helped justify the ramble in the heat.
A quick stroll took us to the NW edge of the lake and the Quyuan Gardens, one of the reputed "New Ten Scenes of West Lake" and home to dozens of species of lotus flowers.
Only a few were in bloom this early in the season,
but the winding bridges and pleasant walkways gave a good view into the landscaping and architectural styles of Chinese gardens.
In the evening it was time for a cruise on the water ferry up to one of the most impressive bridges along the Grand Canal (Gongchen)and a true teahouse experience, a first for Dave and Kathy.
These buy a cup of tea and enjoy the all you can eat buffet extravaganzas are certainly a highlight of living in Zhejiang province,
and this one did not disappoint.
And it helped us sit out the first round of the evening's terrific downpours.
As a testament to the kindness of your average Chinese person, we were apparently a bit aimless in our search for an evening bus and walking through some of the not-so-well lit areas of Hangzhou when we were stopped by a cheerful couple eager to get some pictures with us.
They quickly directed us correctly to our location and we easily caught a bus back towards the hostel.
&nbs;
One thing that captured a lot of time in Hangzhou was free or nearly free museums, which Dave good-naturedly accompanied us for despite his bodies insistence to prolong a not so smooth transition to China...
you just can't keep the man down.
The museums were a testament to some of the goods that China is justifiably proud to have been the first to produce, tea and silk.
Hangzhou is still a well-known producer of these two goods,
and the museums provided a nice look at the crafts.
There was also a traditional Chinese medicine museum/pharmacy that Kathy and Kevin made the trip to.
While the museum confirmed that the trial and error method has allowed traditional medicine to discover uses for all kinds of herbs, spices, plants, animals, bones, etc.
the highlight of the visit was the Ming Dynasty building which housed it.
The architecture of this preserved building amidst a revamped and recreated traditional shopping street was a treat.
&nbs;
Not to be cheated of our time in Hangzhou, it must be noted that Dave and Kathy enjoyed their morning strolls amidst the early to rise citizens in and along the lakeside parks.
It was an aspect of the culture that impressed them to no end, and one of the magnificently beautiful parts of an admirable culture.
There was also the lakeside 13-storied pagoda: Liuhe (Six Harmonies) Pagoda that allowed some nice vistas of the Qiantang River.
&nbs;
After an eventful first week in China it was back to 'work', a return to Anji for some more quality time with our students and a chance for Davey T to show of his lefty penmanship (universally received with eyes-wide, fingers pointing, and shocked gasps of 'waaaahhh') and both members of the parental envoy to test their patience of fielding the same questions repeatedly
(do you like china? What do you think of Chinese food? ______ China?
Always questions about China!!!!!!).
They passed with flying colors, Dave on the receiving end of look alike comparisons with Bill Gates.
They also brought along a secret weapon: a sampler platter of photos from Kevin's childhood.
This led to a vote being taken on short vs. long hair with an underwhelming majority (although a unanimity in certain classes) calling for Kevin to revert to his short haired doo.
He was not to be swayed after having dedicated nearly a year to his locks.
&nbs;
As a note on the amazement that greeted the southpaw stylings, it is much more rare to find a lefty in China due to the constraints of writing characters which require a certain, established stroke order.
This would be complicated by writing with one's left hand.
This makes sense from that perspective
but also attests to the validity of the Chinese saying that "the nail that sticks out gets hammered in".
Another fascinating glimpse was the desire for Chinese students to ask only about China.
It amazed me that more was not asked about the USA, professions, etc.
It was really as if the students were seeking confirmation of the wonders of China, which they have been versed in time and again.
The desire to get feedback from a foreigner is understandable,
but the lack of variety in the questions and subjects was a bit puzzling.
&nbs;
The few days back in Anji gave the parental crew a chance to enjoy the graciousness of Chinese hosts, from being escorted around for a day by QiQi, her sister, and her (boy)friends father who chauffeured them around but incredulously responded to the fact that there would actually be hiking involved...
he abstained, electing for some card games and the base instead of a jaunt.
The highlight of the day (aside from the lavish dinner which provided a taste of Chinese dinner antics, manners, rituals, and rules) was a trip up the Tianhuanping Stored Water Power Pump Station, a joint project with a German team.
The Water Station, at the top of a nifty mountain about 25 kms south of Anji proper uses water flow downhill during the day to supplement electricity needs and pumps the water back up at night when demand is much lower.
&nbs;
The final night included another graciously hosted dinner, this time by the mother of our friend Su Shengkai whom we met eating at a small dumpling place and helped marginally in his transition to University at High Point in North Carolina.
After randomly bumping into her outside the grocery store a dinner was quickly arranged
and the following evening she hosted us at a hotel (and not for the last time!)
and showered Dave and Kathy with some lovely gifts.
What a way to exit Anji! 
To bookend the blitz through a brief foray into China, it was off to experience the small and cozy as well as the unbelievably big and busy that the central coast region has to offer.
First was an overnight trip to the charming and still functional (both points that should not be lost when comparing with other water villages in the region) of Xitang.
This was our second trip to this quaint, cobbled, and canaled village that gained prominence with the arrival of Tom Cruise and the rest of the MI:III film crew to shoot on location.
One evening and morning is enough to enjoy the charm and pace, which remains relaxed enough to encourage art students to stake a waterside spot and craft their craft.
Xitang provides an unparalleled (in our experience) look at how China felt in days gone by.
One highlight was our interaction with one of the abundant and abundantly skilled artisans, this one a fan/scroll painter.
Dave and Kathy ordered one fan of peach blossoms (or at least something resembling them), one of the symbols of longevity.
Before we could get away from Xitang, Kevin and Laurie and ordered another fan and the Tempests had collaborated on a purchase of a mammoth scroll with a rather fierce looking character to head back home to big bro Pete.
&nbs;
After enjoying the virtues of this small water town halfway between Hangzhou and Shanghai, it was back on the highway and on into the big city from where we would bid adieu to the intrepid Tempests.
&nbs;
Our first stop in Shanghai was shopping mecca Najing Lu for an altogether too familiar trip to Pearl City.
It was good for some haggling, gesturing, posturing, and finally a purchase.
Sadly (or not), it wouldn't be our last time in the Pearl Markets.
Nor our second to last (sadly).
From there it was wandering time to the Yu Yuan gardens where we let the wiser members of the party escape the crowds of the bazaar into the gardens and awaited their return at Starbucks...
we've been immersed in the culture for long enough that we don't regard this as a sell-out anymore.
More rambling took us to the riverfront along the Bund, with a look across the Huang Pu river to the futuristic metalscape of Pudong, including the Jin Mao tower (which will soon be topped in height by it's neighbor) and the seemingly crayola inspired orbs of the Oriental Pearl TV Tower.
We dallied as the dusk came and went and the lights began to decorate the night.
Three of us would return in the morning to witness the health conscious out and about, including some troupes of Tai Chi aficionados, some of whom weren't afraid of a Marlboro to accompany the morning grind!
&nbs;
A quick lunch and we unleashed Kathy and Dave to do the museum thing, handle their own metro tickets, and get themselves to the airport.
We have word that they have since then touched down safely in Seattle (
and we have in fact visited them since...
that's how behind we are on this).
The two weeks were action packed
and were intended to prevent a great glimpse into life, history, food, and culture of the China we have experienced and learned about.
It's safe to say we are proud of the smorgasboard we presented
and that this first trip outside of North America was a joyous one for Dave and Kathy/mom and dad.
Too bad more of you didn't come to see us!