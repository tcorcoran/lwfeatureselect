import sys
import json
from pdb import set_trace

#change this:
corenlp_dir = "/Users/summeraccess/corenlp-python/"
sys.path.append(corenlp_dir)
from corenlp import batch_parse
corenlp_dir = corenlp_dir+"stanford-corenlp-full-2013-06-20"

# each post should be its own file in this dir:
text_directory = "/Users/summeraccess/L&W/ML/temp/"
#set_trace()

#this is a generator 

# set_trace()
for p in batch_parse(text_directory+'raw_text/', corenlp_dir):
	with open(text_directory + 'json_files/' + p['file_name']+".json", 'w') as f:
		print text_directory + 'json_files/' + p['file_name']+".json"
		print "wrote: " + p['file_name']
		json.dump(p, f)

