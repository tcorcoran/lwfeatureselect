#!/usr/bin/python
from glob import glob
from pdb import set_trace
from collections import defaultdict
import csv
       
task = 'train'

def mapping(label, maptype):
    """
    Mapping 0
            no mapping
    Mapping 1
          Stative - Local Context -> Stative
          Stative - Consequence -> Stative
          Stative - Implied Action -> Stative
    Mapping 2
          Stative - Local Context -> Orientation
          Stative - Consequence -> Evaluation
          Stative - Implied Action -> Action
    """
    if maptype == 0:
        return label
    if maptype == 1:
        if 'Local' in label : return 'Stative'
        elif 'Consequence' in label: return 'Stative'
        elif 'Implied' in label: return 'Stative'
        else: return label
    elif maptype == 2:
        if 'Local' in label: return 'Orientation'
        elif 'Consequence' in label: return 'Evaluation'
        elif 'Implied' in label: return 'Action'
        else: return label

def merge(task, maptype, results_dir, unigram=False):
    fl = glob(results_dir+ task+ '/*.csv')
    sources = []
    all_fields = []
    for f in fl:
        d = {}
        d['file'] = f
        with open(f, 'rb') as fi:
            reader = csv.DictReader(fi)
            #header = [k for k in reader.next() if not k.startswith('STEM:') or not k.startswith('POS:') and not unigram]
            header = []
            for k in reader.next():
                if k.startswith('STEM:') or k.startswith('POS:') and not unigram:
                    continue
                else:
                    header.append(k)
            columns = defaultdict(list)
            d['fields'] = header
            all_fields.extend(header)
            # set_trace()
            for row in reader:
                for k in row:
                    if k.startswith('Label'):
                        columns[k].append(mapping(row[k], maptype))
                    elif not unigram and  k.startswith("POS:") or k.startswith("STEM:"):
                        continue
                    else:    
                        columns[k].append(row[k])
            d['columns'] = columns
            sources.append(d)


    all_fields = list(set(all_fields))
    for i,t in enumerate(['Label', 'Tree', 'Agreement', 'BoundBegin', 'BoundEnd']):
        all_fields.remove(t)
        all_fields.insert(i,t)

    with open(results_dir + task.upper()+ '.csv', 'wb') as fo:
        writer = csv.writer(fo, delimiter = '\t')
        writer.writerow(all_fields)
        for d in sources:

            story_length = len(d['columns']['Label'])
            for field in all_fields:
                if field not in d['fields']:
                    d['columns'][field] = [0]*story_length
                else:
                    d['columns'][field].reverse()

            for _ in xrange(story_length):

                try:row =  [d['columns'][field].pop() for field in all_fields]
                except: set_trace()
                writer.writerow(row)

for task in ['train', 'test', 'development']:
    merge(task, 2, "results-9-05/" )